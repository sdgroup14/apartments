import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthModalComponent} from './content/templates/auth-modal/auth-modal.component';
import {AuthPageComponent} from './content/pages/auth-page/auth-page.component';
import {LangRuGuard} from './guards/lang-ru.guard';
import {LangUaGuard} from './guards/lang-ua.guard';
import {SavePreloaderComponent} from './content/templates/save-preloader/save-preloader.component';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthPageComponent
  },
  {
    path: 'auth',
    component: AuthModalComponent,
    outlet: 'modals'
  },
  {
    path: 'saveLayout',
    component: SavePreloaderComponent,
    outlet: 'modals'
  },
  {
    path: 'ru',
    canActivate: [LangRuGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./content/pages/main-page/main-page.module').then(mod => mod.MainPageModule)
      },
      {
        path: 'add',
        loadChildren: () => import('./content/pages/add-apartment-page/add-apartment-page.module').then(mod => mod.AddApartmentPageModule)
      },
      {
        path: 'commercial',
        loadChildren: () => import('./content/pages/commercial-page/commercial-page.module').then(mod => mod.CommercialPageModule)
      },
      {
        path: 'sale',
        loadChildren: () => import('./content/pages/sale-page/sale-page.module').then(mod => mod.SalePageModule)
      },
      {
        path: 'rent',
        loadChildren: () => import('./content/pages/rent-page/rent-page.module').then(mod => mod.RentPageModule)
      },
      {
        path: ':city',
        loadChildren: () => import('./content/pages/main-page/main-page.module').then(mod => mod.MainPageModule)
      },
      {
        path: ':city/:action/:obj',
        loadChildren: () => import('./content/pages/apartments-page/apartments-page.module').then(mod => mod.ApartmentsPageModule)
      },
      {
        path: ':city/:action/:obj/:ad',
        loadChildren: () => import('./content/pages/apartment-page/apartment-page.module').then(mod => mod.ApartmentPageModule)
      },
      {
        path: 'edit/:slug',
        loadChildren: () => import('./content/pages/add-apartment-page/add-apartment-page.module').then(mod => mod.AddApartmentPageModule)
      }
    ]
  },
  {
    path: '',
    canActivate: [LangUaGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./content/pages/main-page/main-page.module').then(mod => mod.MainPageModule)
      },
      {
        path: 'add',
        loadChildren: () => import('./content/pages/add-apartment-page/add-apartment-page.module').then(mod => mod.AddApartmentPageModule)
      },
      {
        path: ':city',
        loadChildren: () => import('./content/pages/main-page/main-page.module').then(mod => mod.MainPageModule)
      },
      {
        path: 'commercial',
        loadChildren: () => import('./content/pages/commercial-page/commercial-page.module').then(mod => mod.CommercialPageModule)
      },

      {
        path: 'sale',
        loadChildren: () => import('./content/pages/sale-page/sale-page.module').then(mod => mod.SalePageModule)
      },
      {
        path: 'rent',
        loadChildren: () => import('./content/pages/rent-page/rent-page.module').then(mod => mod.RentPageModule)
      },
      {
        path: ':city/:action/:obj',
        loadChildren: () => import('./content/pages/apartments-page/apartments-page.module').then(mod => mod.ApartmentsPageModule)
      },
      {
        path: ':city/:action/:obj/:ad',
        loadChildren: () => import('./content/pages/apartment-page/apartment-page.module').then(mod => mod.ApartmentPageModule)
      },
      {
        path: 'edit/:slug',
        loadChildren: () => import('./content/pages/add-apartment-page/add-apartment-page.module').then(mod => mod.AddApartmentPageModule)
      },
    ]
  },

  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      initialNavigation: 'enabled',
      onSameUrlNavigation: 'reload',
      preloadingStrategy: PreloadAllModules
      // scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
