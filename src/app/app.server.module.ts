import {NgModule} from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';

import {AppModule} from './app.module';
import {AppComponent} from './app.component';
import {ServerCookiesModule, ServerCookiesService} from '@ngx-utils/cookies/server';
import {TranslateServerLoader} from './services/translate-server-loader.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TransferState} from '@angular/platform-browser';
import {CookiesService} from '@ngx-utils/cookies';

export function translateFactory(transferState: TransferState) {
  return new TranslateServerLoader(transferState);
}

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ModuleMapLoaderModule,
    ServerTransferStateModule,
    ServerCookiesModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateFactory,
        deps: [TransferState]
      }
    })
  ],
  providers: [
    {
    provide: CookiesService,
    useClass: ServerCookiesService
  }
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {
}
