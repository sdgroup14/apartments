import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AddApartmentPageService {

  constructor(private http: HttpClient) {
  }

  getAd(ad_id, action) {
    let urlParams = '?ad_id=' + ad_id;
    urlParams += '&action=' + action;
    return this.http.get(environment.res_api_url + '/ad/show' + urlParams);
  }

  deleteAd(ad_id) {
    return this.http.delete(environment.res_api_url + '/ad/rental/' + ad_id);
  }


  getCategories() {
    return this.http.get(environment.res_api_url + '/dict-categories');

  }


  getProps(action, action_type, obj_id) {
    let urlParams = '?action=' + action + '&obj_id=' + obj_id;
    urlParams += action_type ? '&action_type=' + action_type : '';
    return this.http.get(environment.res_api_url + '/props' + urlParams);
  }

  saveCategories(action, params, obj_id) {
    return this.http.post(environment.res_api_url + '/ad', {action, params, obj_id}, {});
  }

  saveProps(data) {
    return this.http.post(environment.res_api_url + '/ad/update', data, {});
  }

  saveImg(data) {
    const apiData = new FormData();
    for (const key in data) {
      apiData.append(key, data[key]);
    }
    return this.http.post(environment.res_api_url + '/ad/slide', apiData, {});
  }

  getCity(place_id, lat, lng, name) {
    let urlParams = '?place_id=' + place_id;
    urlParams += '&lat=' + lat;
    urlParams += '&lng=' + lng;
    urlParams += '&name=' + name;
    return this.http.get(environment.res_api_url + '/city/data' + urlParams);
  }

  // getMetro(place_id) {
  //   return this.http.get(environment.res_api_url + '/dict_metro/dict_city_district?city_id=' + place_id);
  // }


  deleteSlide(id) {
    return this.http.delete(environment.res_api_url + '/ad/slide/' + id, {});
  }

}
