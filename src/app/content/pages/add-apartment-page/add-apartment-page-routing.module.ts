import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddApartmentPageComponent} from './add-apartment-page.component';


const routes: Routes = [
  {
    path: '',
    component: AddApartmentPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddApartmentPageRoutingModule { }
