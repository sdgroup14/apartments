import {Component, OnDestroy, OnInit} from '@angular/core';
import {AddApartmentPageService} from './services/add-apartment-page.service';
import {Subscription} from 'rxjs';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {take} from 'rxjs/operators';
import {LangService} from '../../../services/lang.service';
import {DateService} from '../../../services/date.service';
import {CommonAppService} from '../../../services/common-app.service';

@Component({
  selector: 'app-add-apartment',
  templateUrl: './add-apartment-page.component.html',
  styleUrls: ['./add-apartment-page.component.scss']
})
export class AddApartmentPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  autocomplete: any = {
    // lat: 48.3140961,
    // lng: 28.9368704
  };

  nav: any = [
    {
      title: 'Тип',
      selected: true
    }
  ];

  props: any = [];

  edit = true;
  basket = false;

  viewData: any = {
    top: {}
  };
  last_type_id = null;
  announce_id = null;
  ad_top_res: any = [];
  districts: any = [];
  metro: any = [];
  ad_top: any = [];
  nextBtn = false;
  topApiData: any = {};

  deleteAnnounce() {
    // this.basket = false;
    // this.save
    // this.router.navigate(['', {outlets: {modals: 'saveLayout'}}], {skipLocationChange: true});
    this.service.deleteAd(this.announce_id).pipe(take(1)).subscribe(resp => {
      console.log(resp);
      // setTimeout(() => {
      //   this.router.navigate(['', {outlets: {modals: null}}], {skipLocationChange: true});
      // }, 200);
      this.router.navigate(['/add']);

    });
  }

  editHandler() {
    if (!this.announce_id) {
      this.edit = true;
      this.viewData.top.action = null;
    } else {
      this.basket = true;
    }
  }

  newAdress(data, section) {
    this.autocomplete = {
      lat: data.place.geometry.location.lat(),
      lng: data.place.geometry.location.lng()
    };
    // console.log(data.place)
    data.place.address_components.map(it => {
      const l = it.types.filter(type => {
        return type === 'route' || 'street_number';
      })[0];
      //
      if (l === 'route') {
        // console.log(l);
        this.autocomplete.route = it.long_name;
      } else if (l === 'street_number') {
        this.autocomplete.house_number = it.long_name;
      }
    });
    // place.vicinity
    // console.log(data.town);
    this.service.getCity(data.town.place_id, data.town.lat, data.town.lng, data.town.long_name).pipe(take(1)).subscribe((resp: any) => {
      console.log(resp);
      // this.districts = resp.result.districts;
      this.districts = [];
      if (resp.result.districts.length) {
        this.districts = resp.result.districts;
        console.log('this.districts', this.districts);
      }
      this.metro = [];
      section.city_id = resp.result.city_id;
      if (resp.result.metro.length) {
        this.metro = resp.result.metro;
        section.metro = [];
        section.metro.push({
          stations: resp.result.metro,
          radio: [
            {
              dep_keys: [],
              ext_label: null,
              id: 1,
              value: 1,
              is_def: true,
              is_final: false,
              name: 'Пешком',
              slug: 'rental'
            },
            {
              dep_keys: [],
              ext_label: null,
              id: 2,
              value: 2,
              is_def: true,
              is_final: false,
              name: 'На авто',
              slug: 'rental'
            }
          ],
          int: {
            id: 8,
            items: [],
            label: '',
            placeholder: null,
            slug: 'area_kitchen',
            type: 'int',
            unit: 'мин.',
            value: ''
          },
          isOpen: true
        });
      }


    });
    // data.place.address_components.map(it => {
    //   // console.log(it);
    //   const l = it.types.filter(type => {
    //     return type === 'locality';
    //   })[0];
    //
    //   if (l) {
    //     // console.log(it.short_name);
    //     this.service.getCity(it.short_name).pipe(take(1)).subscribe((resp: any) => {
    //       console.log(resp);
    //       this.districts = resp.result.districts;
    //       this.metro = resp.result.metro;
    //     });
    //     // console.log(place)
    //   }
    // });

  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  changeRowValue(data, row) {
    if (row.type === 'checkbox_one' || row.type === 'radio') {
      row.value = data.value;
    }
    if (row.type === 'checkbox') {

      const items = row.items.filter(it => {
        return it.selected;
      });
      if (items.length) {
        row.value = '';
        items.map((it, i) => {
          if (i !== items.length - 1) {
            row.value += it.value + ',';
          } else {
            row.value += it.value;
          }
        });
      }
    }
  }

  changeType(data, row) {
    this.changeRowValue(data, row);
    const row_index = this.ad_top.indexOf(row);
    if (row.is_change) {
      const _new_top = this.ad_top.slice(0, row_index + 1);
      this.ad_top = _new_top;
      this.nextBtn = false;
      this.topApiData.object_id = null;

    }
    if (!row_index) {
      this.last_type_id = null;
      this.props = [];
      this.nav.length = 1;
    }
    this.ad_top_res.map(_row => {
      if (data.dep_keys && data.dep_keys.length && data.dep_keys.indexOf(_row.key) >= 0) {
        this.ad_top.push(JSON.parse(JSON.stringify(_row)));
      }
    });

    if (data.is_final && row_index === this.ad_top.length - 1) {
      this.topApiData.object_id = data.id;
      this.viewData.top.object = data.name;
      this.nextBtn = true;
    }
  }

  changeMetroTime(data, metro) {
    metro.int.value = data;
  }

  addMetro(allMetro) {
    if (allMetro.length < 3) {
      allMetro.push({
        stations: this.metro,
        radio: [
          {
            dep_keys: [],
            ext_label: null,
            id: 1,
            value: 1,
            is_def: true,
            is_final: false,
            name: 'Пешком',
            slug: 'rental'
          },
          {
            dep_keys: [],
            ext_label: null,
            id: 2,
            value: 2,
            is_def: true,
            is_final: false,
            name: 'На авто',
            slug: 'rental'
          }
        ],
        int: {
          id: 8,
          items: [],
          label: '',
          placeholder: null,
          slug: 'area_kitchen',
          type: 'int',
          unit: 'мин.',
          value: ''
        }
      });
    }

    // const current = allMetro.indexOf(metro);
    // const next = allMetro[current + 1];
    // const afterNext = allMetro[current + 2];
    // if (metro.int.value && metro.city_metro_id) {
    //   // console.log('open');
    //   if (next) {
    //     next.isOpen = true;
    //   }
    // } else {
    //   console.log('close');
    //   if (next) {
    //     next.isOpen = false;
    //
    //   }
    //   if (afterNext) {
    //     afterNext.isOpen = false;
    //
    //   }
    //
    // }
  }

  deleteMetro(metro, allMetro) {
    const index = allMetro.indexOf(metro);
    allMetro.splice(index, 1);
  }

  changeMetro(data, metro) {
    metro.city_metro_id = data.id;
    // metro.int.value = '';
    // metro.radio = [
    //   {
    //     dep_keys: [],
    //     ext_label: null,
    //     id: 1,
    //     is_def: true,
    //     is_final: false,
    //     name: 'Пешком',
    //     slug: 'rental'
    //   },
    //   {
    //     dep_keys: [],
    //     ext_label: null,
    //     id: 2,
    //     is_def: true,
    //     is_final: false,
    //     name: 'На авто',
    //     slug: 'rental'
    //   }
    // ];
    // this.metroVisibilityHandler(metro, allMetro);
  }


  getSelectedTop() {
    const action = this.ad_top.filter(row => {
      return row.key === 'action';
    })[0];
    const action_item = action.items.filter(it => {
      return it.selected;
    })[0];

    this.topApiData.action_slug = action_item.slug;
    this.viewData.top.action = action_item.name;

    console.log(this.ad_top);
    const action_type = this.ad_top.filter(row => {
      return row.key === 'action';
    })[0];
    const action_type_item = action_type.items.filter(it => {
      return it.selected;
    })[0];
    this.topApiData.action_type_slug = action_type_item.slug;
    this.viewData.top.action_type = action_type_item.name;
  }

  getProps() {
    if (!this.announce_id) {
      this.getSelectedTop();
    }
    this.nav.length = 1;

    this.subscriptions.add(this.service.getProps(this.topApiData.action_slug, this.topApiData.action_type_slug, this.topApiData.object_id).subscribe((resp: any) => {
      this.edit = false;
      if (this.last_type_id !== this.topApiData.action_slug) {
        this.last_type_id = this.topApiData.action_slug;
        this.props = resp.result;
      }
      this.props.map(row => {
        this.nav.push({
          title: row.title,
          selected: false
        });
      });
    }));


  }

  savePropsHandler(create?) {
    this.router.navigate(['', {outlets: {modals: 'saveLayout'}}], {skipLocationChange: true});
    const data: any = {
      action: this.topApiData.action_slug,
      ad_id: this.announce_id,
    };
    this.props.map(row => {
      if (row.key === 'characteristics' || row.key === 'about_building' || row.key === 'description' || row.key === 'cost' || row.key === 'contacts') {
        for (const prop of row.props) {
          if (prop.value) {
            data[prop.slug] = prop.value;
          }
        }
      }
      if (row.key === 'address') {
        data.lat = this.autocomplete.lat;
        data.lng = this.autocomplete.lng;
        data.street = this.autocomplete.route;
        data.house_number = this.autocomplete.house_number;
        data.city_id = row.city_id;
        // console.log(row);


        if (row.city_district_id) {
          data.city_district_id = row.city_district_id;
        }
        if (row.metro && row.metro.length && row.metro[0].hasOwnProperty('int')) {
          data.metro = [];
          row.metro.map(metro => {
            if (metro.int.value && metro.city_metro_id) {
              data.metro.push({
                id: metro.city_metro_id,
                on_foot: metro.radio[0].selected ? true : false,
                time: metro.int.value
              });
            }
          });
          if (data.metro.length) {
            data.metro = JSON.stringify(data.metro);
          } else {
            delete data.metro;
          }
        }
      }
    });
    // console.log(data);

    this.subscriptions.add(this.service.saveProps(data).subscribe((res: any) => {
      setTimeout(() => {
        this.router.navigate(['', {outlets: {modals: null}}], {skipLocationChange: true});
        console.log(res);
        this.common_service.notification.next({
          type: 'succsess',
          text: res.message
        });
      }, 200);
      if (create) {
        setTimeout(() => {
          this.router.navigate(['/edit/', this.announce_id], {queryParams: {action: this.topApiData.action_slug}});
        }, 400);
      }

      const photo_section = this.props.filter(row => {
        return row.key === 'photo';
      })[0];

      if (photo_section.items && photo_section.items.length) {

        if (photo_section.items.length) {
          this.saveImage(photo_section.items[0], photo_section.items);
        }
      } else {
        // this.router.navigate(['', {outlets: {modals: null}}], {skipLocationChange: true});
        // console.log('123')
        // setTimeout(() => {
        // }, 200);
      }
    }));
  }

  saveImage(slide, slides) {
    const data: any = {
      action: this.topApiData.action_slug,
      ad_id: this.announce_id,
      order: slide.order
    };
    if (!slide.hasOwnProperty('id')) {
      data.img = slide.img;
    } else {
      data.id = slide.id;
    }
    this.service.saveImg(data).subscribe((img_res: any) => {
      const i = slides.indexOf(slide);
      if (!slide.id) {
        slide.id = img_res.result.id;
      }
      if (i < slides.length - 1) {
        this.saveImage(slides[i + 1], slides);
      } else {
        setTimeout(() => {
          this.router.navigate(['', {outlets: {modals: null}}], {skipLocationChange: true});
        }, 200);
      }
    });
  }


  submitDraft() {
    // if(!this.announce_id){
    // }
    if (!this.announce_id) {
      this.getSelectedTop();
      this.subscriptions.add(this.service.saveCategories(this.topApiData.action_slug, {}, this.topApiData.object_id).subscribe((resp: any) => {
        this.announce_id = resp.result.ad_id;
        this.viewData.main = resp.result;
        this.savePropsHandler(true);
      }));
    } else {
      this.savePropsHandler();
    }

  }

  constructor(
    private service: AddApartmentPageService,
    private common_service: CommonAppService,
    private route: ActivatedRoute,
    public date: DateService,
    private router: Router,
    private location: Location,
    public lang: LangService,
    private page_spinner: PagePreloaderService
  ) {
  }

  ngOnInit() {
    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params.slug) {
          this.announce_id = params.slug;
          this.topApiData.action_slug = this.route.snapshot.queryParams.action;
          this.topApiData.action_type_slug = this.route.snapshot.queryParams.action_type;
          this.service.getAd(params.slug, this.topApiData.action_slug).subscribe((res: any) => {
            this.props = res.result;
            this.edit = false;
            this.props.map(section => {
              if (section.key === 'main') {
                this.viewData.main = section;
                const keys = section.ad_type_title.split(' / ');

                this.viewData.top.action = keys[0];
                // this.viewData.top.action_type = keys[1];
                this.viewData.top.object = keys[1];
                // console.log(this.viewData);
              } else if (section.key === 'photos' && !section.items) {
                section.items = [];
              } else if (section.key === 'address') {
                setTimeout(() => {
                  this.autocomplete = {
                    lat: section.lat,
                    lng: section.lng,
                    route: section.street,
                    house_number: section.house_number,
                  };
                }, 100);
                const metro_arr = [];
                this.metro = section.dict_metro;
                this.districts = section.dict_city_district;
                const selected_district = this.districts.filter(_d => {
                  return _d.id === section.city_district_id;
                })[0];
                if (selected_district) {
                  selected_district.selected = true;
                }
                section.metro.map(metro => {
                  const _stations = [];
                  section.dict_metro.map(it => {
                    const _m: any = Object.assign({}, it);
                    if (_m.id === metro.id) {
                      _m.selected = true;
                    }
                    _stations.push(_m);
                  });
                  metro_arr.push({
                    stations: _stations,
                    radio: [
                      {
                        dep_keys: [],
                        ext_label: null,
                        id: 1,
                        value: 1,
                        is_def: metro.on_foot,
                        is_final: false,
                        name: 'Пешком',
                        slug: 'rental'
                      },
                      {
                        dep_keys: [],
                        ext_label: null,
                        id: 2,
                        value: 2,
                        is_def: !metro.on_foot,
                        is_final: false,
                        name: 'На авто',
                        slug: 'rental'
                      }
                    ],
                    int: {
                      id: 8,
                      items: [],
                      label: '',
                      placeholder: null,
                      slug: 'area_kitchen',
                      type: 'int',
                      unit: 'мин.',
                      value: metro.time
                    },
                    isOpen: true
                  });
                });
                section.metro = metro_arr;
              } else {
                // console.log(section);
                section.props.map(row => {
                  if (row.type === 'checkbox_one' || row.type === 'radio') {
                    const item = row.items.filter(it => {
                      return it.selected;
                    })[0];
                    if (item) {
                      row.value = item.value;
                    }
                  }
                  if (row.type === 'checkbox') {
                    const items = row.items.filter(it => {
                      return it.selected;
                    });
                    if (items.length) {
                      row.value = '';
                      items.map((it, i) => {
                        if (i !== items.length - 1) {
                          row.value += it.value + ',';
                        } else {
                          row.value += it.value;
                        }
                      });
                    }
                  }
                });
                // setTimeout(() => {
                //   this.getSelectedTop();
                // }, 300);
              }
              this.page_spinner.hide();
            });
          });
        } else {
          // console.log('empty');
          this.subscriptions.add(this.service.getCategories().subscribe((resp: any) => {
            this.ad_top_res = resp.result;
            this.ad_top.push(this.ad_top_res[0]);
            this.page_spinner.hide();
          }));
        }
      })
    );
  }

}
