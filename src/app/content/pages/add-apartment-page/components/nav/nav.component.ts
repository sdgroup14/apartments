import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  @Input() list: any = [];
  //   [
  //   {
  //     title: 'Тип',
  //     selected: true
  //   },
  //   {
  //     title: 'Характеристики',
  //   },
  //   {
  //     title: 'Адрес',
  //   },
  //   {
  //     title: 'Здание',
  //   },
  //   {
  //     title: 'Описание',
  //   },
  //   {
  //     title: 'Фото',
  //   },
  //   {
  //     title: 'Стоимость',
  //   },
  //   {
  //     title: 'Контакты',
  //   }
  // ];

  constructor() {
  }

  ngOnInit() {
  }

}
