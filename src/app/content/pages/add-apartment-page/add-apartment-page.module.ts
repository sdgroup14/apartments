import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddApartmentPageComponent} from './add-apartment-page.component';
import {AddApartmentPageRoutingModule} from './add-apartment-page-routing.module';
import {NavComponent} from './components/nav/nav.component';
import {FormRadioModule} from '../../templates/elements/form-radio/form-radio.module';
import {FormCheckboxModule} from '../../templates/elements/form-checkbox/form-checkbox.module';
import {FormInputNumberModule} from '../../templates/elements/form-input-number/form-input-number.module';
import {FormTextareaModule} from '../../templates/elements/form-textarea/form-textarea.module';
import {FormPhotoModule} from '../../templates/elements/form-photo/form-photo.module';
import {FormPlacesAutocompleteModule} from '../../templates/elements/form-places-autocomplete/form-places-autocomplete.module';
import {FormMapModule} from '../../templates/elements/form-map/form-map.module';
import {FormRadioMultiLineModule} from '../../templates/elements/form-radio-multi-line/form-radio-multi-line.module';
import {FormInputStringModule} from '../../templates/elements/form-input-string/form-input-string.module';
import {FormInputPhoneModule} from '../../templates/elements/form-input-phone/form-input-phone.module';
import {FormSelectModule} from '../../templates/elements/form-select/form-select.module';
import {ClickOutsideModule} from '../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [
    AddApartmentPageComponent,
    NavComponent
  ],
  imports: [
    CommonModule,
    AddApartmentPageRoutingModule,
    FormRadioModule,
    FormCheckboxModule,
    FormRadioMultiLineModule,
    FormInputNumberModule,
    FormTextareaModule,
    FormPhotoModule,
    FormPlacesAutocompleteModule,
    FormMapModule,
    FormInputStringModule,
    FormInputPhoneModule,
    FormSelectModule,
    ClickOutsideModule
  ]
})
export class AddApartmentPageModule { }
