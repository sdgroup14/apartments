import {Component, OnDestroy, OnInit} from '@angular/core';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-commercial-page',
  templateUrl: './commercial-page.component.html',
  styleUrls: ['./commercial-page.component.scss']
})
export class CommercialPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(private page_spinner: PagePreloaderService) { }

  ngOnInit() {
    this.page_spinner.hide();
  }

}
