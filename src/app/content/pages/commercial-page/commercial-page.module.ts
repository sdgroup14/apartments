import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommercialPageRoutingModule } from './commercial-page-routing.module';
import {CommercialPageComponent} from './commercial-page.component';


@NgModule({
  declarations: [CommercialPageComponent],
  imports: [
    CommonModule,
    CommercialPageRoutingModule
  ]
})
export class CommercialPageModule { }
