import {Component, Input, OnInit} from '@angular/core';
import {PhoneService} from '../../../../../services/filters/phone.service';

@Component({
  selector: 'app-info-widget',
  templateUrl: './info-widget.component.html',
  styleUrls: ['./info-widget.component.scss']
})
export class InfoWidgetComponent implements OnInit {
  @Input() ad: any = {};

  getPhone() {
    this.phone_service.getPhone(this.ad, this.ad.action_id);
  }

  constructor(private phone_service: PhoneService) {
  }

  ngOnInit() {
  }

}
