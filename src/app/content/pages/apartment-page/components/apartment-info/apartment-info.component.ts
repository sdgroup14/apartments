import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

// import {Swiper} from 'swiper/dist/js/swiper.esm';

@Component({
  selector: 'app-apartment-info',
  templateUrl: './apartment-info.component.html',
  styleUrls: ['./apartment-info.component.scss']
})
export class ApartmentInfoComponent implements OnInit, OnChanges {
  @Input() list: any = [];
  columns: any = [];
  @Input() columns_length = 0;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.list.currentValue && changes.list.currentValue.length) {
      // console.log('list');
      this.columns = [];
      let ind = 0;
      this.list.filter(it => {return it.value}).map(it => {
        if (!this.columns[ind]) {
          this.columns[ind] = [];
        }
        this.columns[ind].push(it);
        ind++;
        if (ind >= this.columns_length) {
          ind = 0;
        }
      });
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
