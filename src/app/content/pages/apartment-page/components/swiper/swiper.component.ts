import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Swiper, Navigation} from 'swiper/dist/js/swiper.esm.js';

Swiper.use([Navigation]);

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss']
})
export class SwiperComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('swiper', {static: false}) swiper: ElementRef;
  @ViewChild('btnPrev', {static: false}) btnPrev: ElementRef;
  @ViewChild('btnNext', {static: false}) btnNext: ElementRef;
  currnetIndex = 1;
  @Input() data: any = {};

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data.currentValue) {
      setTimeout(() => {
        const swiper = new Swiper(this.swiper.nativeElement, {
          navigation: {
            nextEl: this.btnNext.nativeElement,
            prevEl: this.btnPrev.nativeElement
          },
          autoHeight: true
        });

        swiper.on('slideChange', (i) => {
          // console.log('slide changed', );
          this.currnetIndex = swiper.realIndex + 1;
        });
      });

    }
  }

  ngAfterViewInit() {
    // const swiper = new Swiper(this.swiper.nativeElement, {
    //   navigation: {
    //     nextEl: this.btnNext.nativeElement,
    //     prevEl: this.btnPrev.nativeElement
    //   },
    //   autoHeight: true
    // });
    //
    // swiper.on('slideChange', (i) => {
    //   // console.log('slide changed', );
    //   this.currnetIndex = swiper.realIndex + 1;
    // });
  }

  ngOnInit() {

  }

}
