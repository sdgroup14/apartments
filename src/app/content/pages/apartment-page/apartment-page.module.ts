import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApartmentPageRoutingModule } from './apartment-page-routing.module';
import {ApartmentPageComponent} from './apartment-page.component';
import {AuthorWidgetComponent} from './components/author-widget/author-widget.component';
import {InfoWidgetComponent} from './components/info-widget/info-widget.component';
import {SwiperComponent} from './components/swiper/swiper.component';
import {ApartmentInfoComponent} from './components/apartment-info/apartment-info.component';
import {FacilitiesComponent} from './components/facilities/facilities.component';
import {PhonePipeModule} from '../../../pipes/phone-pipe/phone-pipe.module';


@NgModule({
  declarations: [
    ApartmentPageComponent,
    AuthorWidgetComponent,
    InfoWidgetComponent,
    SwiperComponent,
    ApartmentInfoComponent,
    FacilitiesComponent
  ],
  imports: [
    CommonModule,
    ApartmentPageRoutingModule,
    PhonePipeModule
  ]
})
export class ApartmentPageModule { }
