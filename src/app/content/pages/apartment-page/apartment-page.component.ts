import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';
import {ApartmentPageService} from './services/apartment-page.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-apartment-page',
  templateUrl: './apartment-page.component.html',
  styleUrls: ['./apartment-page.component.scss']
})
export class ApartmentPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  ad: any = {};

  constructor(
    private page_spinner: PagePreloaderService,
    private service: ApartmentPageService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnDestroy() {
    // this.common.topNav.next([]);
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.page_spinner.hide();
    this.subscriptions.add(this.route.params.subscribe(params => {
      this.subscriptions.add(this.service.getAd(params.action, params.ad).subscribe((resp: any) => {
        this.ad = resp.result.ad;
        // console.log(this.ad);
      }));
    }));
  }

}
