import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApartmentPageComponent} from './apartment-page.component';


const routes: Routes = [
  {
    path: '',
    component: ApartmentPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApartmentPageRoutingModule { }
