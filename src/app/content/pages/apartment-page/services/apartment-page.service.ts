import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApartmentPageService {


  getAd(action_id, ad_slug) {
    let urlParams = action_id ? '?action_id=' + action_id + '&' : '?';
    urlParams += ad_slug ? 'slug=' + ad_slug : '';
    return this.http.get(environment.api + '/ad' + urlParams);
  }


  // https://nm-api.wander.black/v1/filters
  constructor(private http: HttpClient) {
  }

}
