import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SalePageRoutingModule} from './sale-page-routing.module';
import {SalePageComponent} from './sale-page.component';


@NgModule({
  declarations: [SalePageComponent],
  imports: [
    CommonModule,
    SalePageRoutingModule
  ]
})
export class SalePageModule {
}
