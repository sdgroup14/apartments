import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';

@Component({
  selector: 'app-sale-page',
  templateUrl: './sale-page.component.html',
  styleUrls: ['./sale-page.component.scss']
})
export class SalePageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(private page_spinner: PagePreloaderService) { }

  ngOnInit() {
    this.page_spinner.hide();
  }

}
