import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';

@Component({
  selector: 'app-radio-with-animation',
  templateUrl: './radio-with-animation.component.html',
  styleUrls: ['./radio-with-animation.component.scss']
})
export class RadioWithAnimationComponent implements OnInit, OnChanges {
  @ViewChild('layoutEl', {static: false}) layoutEl: ElementRef;
  @ViewChild('boxEl', {static: false}) boxEl: ElementRef;
  @Output() changeAction = new EventEmitter();
  @Input() btns: any = [];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.btns.currentValue && changes.btns.currentValue.length) {
      // this.count = this.maxLength - this.value.length;
      // console.log('change');
      const selected_btn = this.btns.filter(btn => {
        return btn.is_def;
      })[0];
      if (selected_btn) {
        selected_btn.selected = true;
        setTimeout(() => {
          this.checkSelected();
        }, 100);
      }
    }
  }

  layoutPosition(btn) {
    // console.log(item.offsetLeft);
    const el = this.layoutEl.nativeElement;
    el.style.width = btn.offsetWidth + 'px';
    el.style.left = btn.offsetLeft + 'px';
  }

  checkSelected(i?) {
    let index = 0;
    if (typeof i === 'number') {
      this.btns[i].selected = true;
      this.btns.map((b, _i) => {
        if (i !== _i) {
          b.selected = false;
        }
      });
      index = i;
      this.changeAction.emit(this.btns[i]);
    } else {
      const selected = this.btns.filter(_btn => {
        return _btn.selected;
      })[0];
      index = this.btns.indexOf(selected);
      this.changeAction.emit(selected);
    }
    const btn = this.boxEl.nativeElement.children[index];
    // console.log(btn)
    this.layoutPosition(btn);
  }

  constructor() {
  }

  // ngAfterViewInit() {
  // }

  ngOnInit() {
  }

}
