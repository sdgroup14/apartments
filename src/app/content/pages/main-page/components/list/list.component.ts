import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() photo: string;
  list: any = [
    {
      title: '1-комнатные'
    },
    {
      title: 'Квартиры-студии'
    },
    {
      title: 'Комната в общежитии'
    },
    {
      title: '1-комнатные'
    },
    {
      title: 'Квартиры-студии'
    },
    {
      title: 'Комната в общежитии'
    },
    {
      title: '1-комнатные'
    },

  ];

  constructor() {
  }

  ngOnInit() {
  }

}
