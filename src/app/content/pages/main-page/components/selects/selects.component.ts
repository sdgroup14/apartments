import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LangService} from '../../../../../services/lang.service';
import {FiltersObjectService} from '../../../../../services/filters/filters-object.service';

@Component({
  selector: 'app-selects',
  templateUrl: './selects.component.html',
  styleUrls: ['./selects.component.scss']
})
export class SelectsComponent implements OnInit {
  // isPlaceDropdown = false;
  @Input() filters: any = [];
  @Output() selectObject = new EventEmitter();
  @Output() selectPlace = new EventEmitter();
  @Output() changeProps = new EventEmitter();
  place: any = {
    name: 'Киев',
    isOpen: false,
    id: 154
  };

  changeObject(obj, row, filter) {
    this.obj_service.selectObjectItem(obj, row, filter);
    this.selectObject.emit({obj, filter})
  }

  changeCheckbox(data, filter) {
    filter.value = '';
    const items = filter.items.filter(it => {
      return it.selected;
    });
    if (items.length) {
      filter.title = '';
      items.map((it, i) => {
        if (i !== items.length - 1) {
          filter.value += it.value + ',';
          filter.title += it.name + ',';
        } else {
          filter.value += it.value;
          filter.title += it.name;
        }
      });
    } else {
      filter.title = filter.placeholder;
    }
    this.changeProps.emit(true)
    // filter.value = data.id;
    // console.log(filter);
    // this.selectObject.emit(data);
  }


  changePlace(data) {
    if (this.place.isOpen) {
      this.place.isOpen = false;
    }
    this.place = data;
    this.selectPlace.emit(this.place);
  }

  constructor(public lang: LangService, private obj_service: FiltersObjectService) {
  }

  ngOnInit() {
  }

}
