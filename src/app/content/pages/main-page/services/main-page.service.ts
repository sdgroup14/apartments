import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MainPageService {


  constructor(private http: HttpClient) {
  }

  getParams() {
    return this.http.get(environment.res_api_url + '/search-widget/main-conf');
  }

  // changeAction(slug) {
  //   return this.http.get(environment.res_api_url + '/search-widget/objects-list?action_slug=' + slug);
  // }

  //
  // getGroups(action_slug, group_slug) {
  //   const urlParams = '?action_slug=' + action_slug + '&group_slug=' + group_slug;
  //   return this.http.get(environment.res_api_url + '/search-widget/group-objects-list' + urlParams);
  // }
  //
  // getPlace(search) {
  //   return this.http.get(environment.res_api_url + '/city/dict?search=' + search);
  // }

  //
}
