import {Component, OnDestroy, OnInit} from '@angular/core';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';
import {Subscription} from 'rxjs';
import {PlatformService} from '../../../services/platform.service';
import {MainPageService} from './services/main-page.service';
import {Router} from '@angular/router';
import {FiltersObjectService} from '../../../services/filters/filters-object.service';
import {FiltersAdCountService} from '../../../services/filters/filters-ad-count.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  title = 'My first AGM project';
  lat = 51.678418;
  lng = 7.809007;
  isBrowser = false;
  top: any = [];
  filters: any = [];
  props: any = {};
  objects: any = {};
  data: any = {
    place_id: 154,
    place_slug: 'kyiv'
  };
  count = 0;
  countIsLoading = true;

  changeObject(data) {
    this.obj_service.changeObject(data, this.filters, this.data, this.props);
    this.getCount();
  }

  changePlace(place) {
    this.data.place_slug = place.slug;
    this.data.place_id = place.id;
    this.getCount();
  }

  changeAction(action) {
    action.dep_slug.map(key => {
      const new_item = this.objects[key];
      if (new_item) {
        this.filters = [new_item];
      }
    });
    console.log('action');
    this.data.action_slug = action.slug;
    this.data.action_id = action.id;
    // this.getCount();
  }

  getCount() {
    setTimeout(() => {
      this.countIsLoading = true;
    });
    // const params: any = {};
    // this.filters.filter(filter => {
    //   return filter.isProp;
    // }).map(f => {
    //   if (f.value) {
    //     params[f.slug] = f.value;
    //   }
    // });
    console.log(this.data.city_id)
    this.count_service.get(
      this.data.place_id,
      this.data.action_id,
      this.data.obj_id,
      this.filters,
      []
    );
    // this.count_service.get(this.data.place_id, this.data.action_id, this.data.obj_id, params, {}).subscribe((resp: any) => {
    //   this.count = resp.result.count;
    //   setTimeout(() => {
    //     this.countIsLoading = false;
    //   }, 150);
    // });
  }

  // getParams = {}

  showResult() {
    const params: any = {};
    this.filters.filter(filter => {
      return filter.isProp;
    }).map(f => {
      if (f.value) {
        params[f.alias] = f.value;
      }
    });
    // console.log(this.data);
    this.router.navigate(['', this.data.place_slug, this.data.action_slug, this.data.obj_slug], {queryParams: params});
    // console.log(this.data);
  }

  ngOnDestroy() {
    this.count_service.value.next(0)
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private page_spinner: PagePreloaderService,
    private platform: PlatformService,
    private router: Router,
    private service: MainPageService,
    private obj_service: FiltersObjectService,
    private count_service: FiltersAdCountService
  ) {
  }

  ngOnInit() {
    this.page_spinner.hide();
    this.isBrowser = this.platform.check();

    this.service.getParams().subscribe((resp: any) => {
      this.top = resp.result.actions.items;
      this.filters = [];
      this.props = resp.result.filters;
      for (const prop in this.props) {
        if (this.props[prop].type === 'checkbox') {
          this.props[prop].title = this.props[prop].placeholder;
        }
      }
      this.objects = resp.result.objects;
    });

    this.subscriptions.add(this.count_service.value.subscribe((value: any) => {
      this.count = value;
      setTimeout(() => {
        this.countIsLoading = false;
      }, 150);
    }));
  }

}
