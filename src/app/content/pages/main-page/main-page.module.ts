import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainPageRoutingModule} from './main-page-routing.module';
import {MainPageComponent} from './main-page.component';
import {ListComponent} from './components/list/list.component';
import {RadioWithAnimationComponent} from './components/radio-with-animation/radio-with-animation.component';
import {SelectsComponent} from './components/selects/selects.component';
import {AgmCoreModule} from '@agm/core';
import {DropdownWithSearchModule} from '../../templates/dropdown-with-search/dropdown-with-search.module';
import {ClickOutsideModule} from '../../../directives/click-outside/click-outside.module';
import {FormRadioMultiLineModule} from '../../templates/elements/form-radio-multi-line/form-radio-multi-line.module';
import {FormCheckboxModule} from '../../templates/elements/form-checkbox/form-checkbox.module';
import {FormInputNumberModule} from '../../templates/elements/form-input-number/form-input-number.module';


@NgModule({
  declarations: [MainPageComponent, ListComponent, RadioWithAnimationComponent, SelectsComponent],
  imports: [
    CommonModule,
    DropdownWithSearchModule,
    ClickOutsideModule,
    MainPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: ''
    }),
    FormRadioMultiLineModule,
    FormCheckboxModule,
    FormInputNumberModule

  ]
})
export class MainPageModule {
}
