import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RentPageRoutingModule } from './rent-page-routing.module';
import {RentPageComponent} from './rent-page.component';


@NgModule({
  declarations: [RentPageComponent],
  imports: [
    CommonModule,
    RentPageRoutingModule
  ]
})
export class RentPageModule { }
