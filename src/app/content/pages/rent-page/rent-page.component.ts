import {Component, OnDestroy, OnInit} from '@angular/core';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-rent-page',
  templateUrl: './rent-page.component.html',
  styleUrls: ['./rent-page.component.scss']
})
export class RentPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(private page_spinner: PagePreloaderService) { }

  ngOnInit() {
    this.page_spinner.hide();
  }

}
