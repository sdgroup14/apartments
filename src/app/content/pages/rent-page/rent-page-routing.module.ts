import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RentPageComponent} from './rent-page.component';


const routes: Routes = [
  {
    path: '',
    component: RentPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RentPageRoutingModule { }
