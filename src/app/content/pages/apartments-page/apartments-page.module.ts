import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ApartmentsPageRoutingModule} from './apartments-page-routing.module';
import {ApartmentsPageComponent} from './apartments-page.component';
import {TopComponent} from './components/top/top.component';
import {DropdownWithSearchModule} from '../../templates/dropdown-with-search/dropdown-with-search.module';
import {ClickOutsideModule} from '../../../directives/click-outside/click-outside.module';
import {FiltersComponent} from './components/filters/filters.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {BtnCloseModule} from '../../templates/btn-close/btn-close.module';
import {ListComponent} from './components/list/list.component';
import {FormCheckboxModule} from '../../templates/elements/form-checkbox/form-checkbox.module';
import {FormRadioModule} from '../../templates/elements/form-radio/form-radio.module';
import {FormInputNumberModule} from '../../templates/elements/form-input-number/form-input-number.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {DatePipeModule} from '../../../pipes/date-pipe/date-pipe.module';
import {FormRadioMultiLineModule} from '../../templates/elements/form-radio-multi-line/form-radio-multi-line.module';
import {PhonePipeModule} from '../../../pipes/phone-pipe/phone-pipe.module';


@NgModule({
  declarations: [ApartmentsPageComponent, TopComponent, FiltersComponent, ListComponent],
  imports: [
    CommonModule,
    ApartmentsPageRoutingModule,
    DropdownWithSearchModule,
    ClickOutsideModule,
    PerfectScrollbarModule,
    BtnCloseModule,
    FormCheckboxModule,
    FormRadioModule,
    FormInputNumberModule,
    NgxPaginationModule,
    DatePipeModule,
    FormRadioMultiLineModule,
    PhonePipeModule
  ]
})
export class ApartmentsPageModule {
}
