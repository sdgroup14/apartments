import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ApartmentsService} from './services/apartments.service';
import {PagePreloaderService} from '../../templates/page-preloader/page-preloader.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {take} from 'rxjs/operators';
import {CommonAppService} from '../../../services/common-app.service';
import {LangService} from '../../../services/lang.service';
import {FiltersObjectService} from '../../../services/filters/filters-object.service';
import {FiltersAdCountService} from '../../../services/filters/filters-ad-count.service';

@Component({
  selector: 'app-apartments-page',
  templateUrl: './apartments-page.component.html',
  styleUrls: ['./apartments-page.component.scss']
})
export class ApartmentsPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild('titleEl', {static: false}) titleEl: ElementRef;
  filters: any = [];
  props: any = {};
  objects: any = {};
  moreFilters: any = [];
  list: any = [];
  title = '';
  // countIsLoading = false;
  filterParams: any = {
    page: 1,
    total: 0,
    city_id: 154,
    order_by: 'datetime',
    direction: 'desc',
  };
  count = 0;

  // data: any = {};

  constructor(
    private service: ApartmentsService,
    private obj_service: FiltersObjectService,
    private count_service: FiltersAdCountService,
    private common: CommonAppService,
    private lang: LangService,
    private router: Router,
    private page_spinner: PagePreloaderService,
    private route: ActivatedRoute
  ) {
  }

  ngOnDestroy() {
    // this.common.topNav.next([]);
    this.count_service.value.next(0);
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  changeObject(data) {
    this.filterParams.obj_slug = data.obj.slug;
    this.obj_service.changeObject(data, this.filters, this.filterParams, this.props);
  }

  changePageHandler(page) {
    this.filterParams.last_page = this.filterParams.page;
    this.filterParams.page = page;
    this.updateList();
  }

  filterHandler(type) {
    if (type === this.filterParams.order_by) {
      this.filterParams.direction = this.filterParams.direction === 'desc' ? 'asc' : 'desc';
    } else {
      this.filterParams.direction = 'asc';
      this.filterParams.order_by = type;
    }
    this.updateList();
  }

  updateList() {

    const ext = {};
    // console.log(this.moreFilters)
    this.moreFilters.filter(filter => {
      return filter.value;
    }).map(f => {
      ext[f.alias] = f.value;
    });
    // console.log(ext)
    this.service.getListNext(this.filterParams, this.route.snapshot.queryParams, ext);
  }

  changeAction(action) {
    action.dep_slug.map(key => {
      const new_item = this.objects[key];

      if (new_item) {
        this.filters.length = 1;
        this.filters.push(new_item);
      }
    });
    this.filterParams.action = action.slug;
    this.filterParams.action_id = action.id;
  }

  showResult(filters) {
    this.moreFilters = filters;
    const params: any = {};
    this.filters.filter(filter => {
      return filter.isProp;
    }).map(f => {
      if (f.value) {
        params[f.alias] = f.value;
      }
    });
    // console.log(params)
    this.router.navigate(['', this.filterParams.city_slug, this.filterParams.action_slug, this.filterParams.obj_slug], {queryParams: params});
    this.updateList();
  }


  ngOnInit() {
    this.page_spinner.hide();
    this.subscriptions.add(this.route.params.subscribe(params => {
      // console.log(params);
      this.service.checkType(params.city, params.action, params.obj).pipe(take(1)).subscribe((resp: any) => {
        // this.params = params;
        // console.log(resp);
        if (resp.success) {
          this.filterParams.action_slug = params.action;
          this.filterParams.city_slug = params.city;
          this.filterParams.obj_slug = params.obj;
          this.filterParams.action_id = resp.result.action.id;
          this.filterParams.city_id = resp.result.city.id;
          this.filterParams.obj_id = resp.result.object.id;

          const route_params = this.route.snapshot.queryParams;
          // console.log(this.route.snapshot.queryParams);
          this.service.getParams().subscribe((res: any) => {
            // this.common.topNav.next(res.result.actions.items);
            // this.filters = [];
            this.props = res.result.filters;
            for (const prop in this.props) {
              const filter = this.props[prop];
              for (const param in route_params) {
                if (param === filter.alias) {
                  filter.value = route_params[param];
                }
              }
              if (filter.type === 'checkbox') {
                if (filter.value) {
                  filter.title = '';
                  filter.value.split(',').map((val, i) => {
                    const items = filter.items.filter(_it => {
                      return +_it.value === +val;
                    });
                    if (items.length) {
                      items[0].selected = true;
                      if (i) {
                        filter.title += ', ' + items[0].name;
                      } else {
                        filter.title += items[0].name;
                      }
                    }
                  });
                } else {
                  filter.title = filter.placeholder;
                }
              }
            }
            this.objects = res.result.objects;
            res.result.actions.items.map(_it => {
              if (_it.slug === params.action) {
                // console.log('select');
                _it.selected = true;
                _it.is_def = true;
                res.result.actions.title = _it.name;
                res.result.actions.value = _it.slug;
                // this.params.action_id = _it.id;
                // this.filterParams.action_id = _it.id;
                this.filters = [res.result.actions];
                _it.dep_slug.map(key => {
                  // console.log(key);
                  const new_item = this.objects[key];
                  // console.log(new_item);
                  new_item.items.map(row => {
                    row.items.map(it => {
                      if (it.slug === params.obj) {
                        // console.log('it', it);
                        // this.params.obj_id = it.id;
                        it.selected = true;
                        it.is_def = true;
                        new_item.value = it.slug;
                        new_item.title = it.name;
                      } else {
                        it.selected = false;
                        it.is_def = false;
                      }
                    });
                  });
                });
              } else {
                _it.selected = false;
                _it.is_def = false;
              }
            });
            const lang = this.lang.get() === 'ru' ? '/ru' : '';
            this.subscriptions.add(this.service.getAds(this.filterParams, route_params).subscribe((_res: any) => {
              _res.result.list.map(link => {
                link.url = lang + '/' + this.filterParams.city_slug + '/' + this.filterParams.action_slug + '/' + this.filterParams.obj_slug + '/' + link.slug;
              });
              this.list = _res.result.list;
              this.filterParams.itemsPerPage = _res.result.per_page;
              this.filterParams.total = _res.result.total;
              this.count_service.value.next(this.filterParams.total);
              this.title = _res.result.title;
            }));
            this.subscriptions.add(this.service.getPageData(params.city, params.action, params.obj).subscribe(_res => {
              // console.log(_res);
            }));
          });
        }
      });
    }));
    this.subscriptions.add(this.service.ads.subscribe((resp: any) => {
      const lang = this.lang.get() === 'ru' ? '/ru' : '';
      resp.result.list.map(link => {
        link.url = lang + '/' + this.filterParams.city_slug + '/' + this.filterParams.action_slug + '/' + this.filterParams.obj_slug + '/' + link.slug;
      });
      this.list = resp.result.list;
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      if (this.filterParams.last_page !== this.filterParams.page) {
        this.filterParams.last_page = this.filterParams.page;
        this.titleEl.nativeElement.scrollIntoView({behavior: 'smooth'});
      }
    }));

  }

}
