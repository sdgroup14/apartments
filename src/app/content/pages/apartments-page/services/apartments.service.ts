import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApartmentsService {
  ads = new Subject();

  getMoreFilters(action, object) {
    let urlParams = '?action_id=' + action;
    urlParams += '&obj_id=' + object;
    return this.http.get(environment.res_api_url + '/search-widget/more-filters' + urlParams);
  }

  getParams() {
    return this.http.get(environment.res_api_url + '/search-widget/main-conf');
  }

  getPageData(city, action, object) {
    // /entities/check-slug
    let urlParams = city ? '?city=' + city + '&' : '?';
    urlParams += action ? 'action=' + action : '';
    urlParams += object ? '&object=' + object : '';
    return this.http.get(environment.api + '/page-data' + urlParams);
  }


  checkType(city, action, object) {
    // /entities/check-slug
    let urlParams = city ? '?city=' + city + '&' : '?';
    urlParams += action ? 'action=' + action : '';
    urlParams += object ? '&object=' + object : '';
    return this.http.get(environment.res_api_url + '/entities/check-slug' + urlParams);
  }

  getAds(params, filters) {
    console.log(filters)
    let urlParams = params.page ? '?page=' + params.page + '&' : '?';
    urlParams += params.city_id ? 'city_id=' + params.city_id : '';
    urlParams += params.action_id ? '&action_id=' + params.action_id : '';
    urlParams += params.obj_id ? '&object_id=' + params.obj_id : '';
    urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    urlParams += '&filters=' + JSON.stringify(filters);
    return this.http.get(environment.api + '/ad-list' + urlParams);
  }

  getListNext(params, filters, ext_filters) {
    let urlParams = params.page ? '?page=' + params.page + '&' : '?';
    urlParams += params.city_id ? 'city_id=' + params.city_id : '';
    urlParams += params.action_id ? '&action_id=' + params.action_id : '';
    urlParams += params.obj_id ? '&object_id=' + params.obj_id : '';
    urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    urlParams += '&filters=' + JSON.stringify(filters);
    urlParams += '&ext_filters=' + JSON.stringify(ext_filters);
    // // urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    // // urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    // // urlParams += params.party_id ? '&party_id=' + params.party_id : '';
    // // urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    // urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    // urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    // urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    // urlParams += params.district_id ? '&district_id=' + params.district_id : '';
    // urlParams += params.workplace_ext_id ? '&workplace_ext_id=' + params.workplace_ext_id : '';
    // urlParams += params.search !== '' ? '&search=' + params.search : '';
    // urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    // urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    // urlParams += params.is_improve !== '' ? '&is_improve=' + params.is_improve : '';
    return this.http.get(environment.api + '/ad-list' + urlParams).subscribe(resp => {
      this.ads.next(resp);
    });
  }


  // https://nm-api.wander.black/v1/filters
  constructor(private http: HttpClient) {
  }
}
