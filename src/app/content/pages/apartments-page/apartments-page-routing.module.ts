import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ApartmentsPageComponent} from './apartments-page.component';


const routes: Routes = [
  {
    path: '',
    component: ApartmentsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApartmentsPageRoutingModule {
}
