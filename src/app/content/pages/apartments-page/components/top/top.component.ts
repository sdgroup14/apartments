import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FiltersObjectService} from '../../../../../services/filters/filters-object.service';
import {ApartmentsService} from '../../services/apartments.service';
import {take} from 'rxjs/operators';
import {FiltersCheckboxService} from '../../../../../services/filters/filters-checkbox.service';
import {Router} from '@angular/router';
import {FiltersAdCountService} from '../../../../../services/filters/filters-ad-count.service';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {
  @Input() filters: any = [];
  @Output() selectObject = new EventEmitter();
  @Output() selectPlace = new EventEmitter();
  @Output() showResults = new EventEmitter();
  @Output() selectAction = new EventEmitter();
  place: any = {
    name: 'Киев',
    slug: 'kyiv',
    id: 154
  };
  @Input() data: any = {};
  moreBtn = false;
  moreFilters: any = [];
  @Output() moreFiltersChange = new EventEmitter();

  showMoreFilters() {
    this.moreBtn = !this.moreBtn;
    // console.log(this.data);
    if (this.moreBtn) {
      this.service.getMoreFilters(this.data.action_id, this.data.obj_id).pipe(take(1)).subscribe((resp: any) => {
        // console.log(resp);
        this.moreFilters = [];
        for (const item in resp.result) {
          this.moreFilters.push(resp.result[item]);
        }
        this.moreFiltersChange.emit(this.moreFilters);
        // console.log(this.moreFilters);
      });
    }
  }

  changeAction(filter, data) {
    filter.d_isOpen = false;
    filter.title = data.name;
    this.selectAction.emit(data);
    this.getCount();
  }

  changePlace(data) {
    if (this.place.isOpen) {
      this.place.isOpen = false;
    }
    this.place = data;
    this.selectPlace.emit(this.place);
    this.getCount();
  }

  changeObject(obj, row, filter) {
    this.obj_service.selectObjectItem(obj, row, filter);
    this.selectObject.emit({obj, filter});
    this.getCount();
  }

  getCount() {
    // this.countIsLoading = true;
    this.count_service.get(
      this.data.city_id,
      this.data.action_id,
      this.data.obj_id,
      this.filters,
      this.moreFilters
    );
  }


  changeCheckbox(data, filter) {
    filter.value = '';
    const items = filter.items.filter(it => {
      return it.selected;
    });
    if (items.length) {
      filter.title = '';
      items.map((it, i) => {
        if (i !== items.length - 1) {
          filter.value += it.value + ',';
          filter.title += it.name + ',';
        } else {
          filter.value += it.value;
          filter.title += it.name;
        }
      });
    } else {
      filter.title = filter.placeholder;
    }
    this.getCount();
  }

  constructor(
    private count_service: FiltersAdCountService,
    private obj_service: FiltersObjectService,
    private router: Router,
    private checkbox_service: FiltersCheckboxService,
    private service: ApartmentsService) {
  }

  ngOnInit() {
  }

}
