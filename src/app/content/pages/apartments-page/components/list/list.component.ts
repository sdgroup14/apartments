import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../../../services/date.service';
import {ApartmentsService} from '../../services/apartments.service';
import {PhoneService} from '../../../../../services/filters/phone.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() list: any = [];
  @Input() filterParams: any = {};

  // @Input() params: any = {};

  getPhone(ad, event) {
    event.stopPropagation();
    this.phone_service.getPhone(ad, this.filterParams.action_id);
  }


  constructor(
    private date: DateService,
    private service: ApartmentsService,
    private phone_service: PhoneService
  ) {
  }

  ngOnInit() {
  }

}
