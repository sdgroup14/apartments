import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FiltersAdCountService} from '../../../../../services/filters/filters-ad-count.service';
import {ActivatedRoute} from '@angular/router';
import {take} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @Output() close = new EventEmitter();
  @Output() showResults = new EventEmitter();
  @Input() data: any = {};
  @Input() mainFilters: any = [];

  @Input() set setFilters(filters) {
    // console.log(filters);
    if (filters.length) {
      // console.log(filters);
      this.filters = filters;
    }
  }

  countIsLoading = false;
  count = 0;
  filters: any = [];

  changeType(data, row) {
    row.value = data.value;
    this.getCount();
    // this.changeRowValue(data, row);
    // const row_index = this.ad_top.indexOf(row);
    // if (row.is_change) {
    //   const _new_top = this.ad_top.slice(0, row_index + 1);
    //   this.ad_top = _new_top;
    //   this.nextBtn = false;
    //   this.topApiData.object_id = null;
    //
    // }
    // if (!row_index) {
    //   this.last_type_id = null;
    //   this.props = [];
    //   this.nav.length = 1;
    // }
    // this.ad_top_res.map(_row => {
    //   if (data.dep_keys && data.dep_keys.length && data.dep_keys.indexOf(_row.key) >= 0) {
    //     this.ad_top.push(JSON.parse(JSON.stringify(_row)));
    //   }
    // });
    //
    // if (data.is_final && row_index === this.ad_top.length - 1) {
    //   this.topApiData.object_id = data.id;
    //   this.viewData.top.object = data.name;
    //   this.nextBtn = true;
    // }
  }

  ngOnDestroy() {
    // this.common.topNav.next([]);
    // this.count_service.value.next(0);
    // this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  changeCheckbox(data, filter) {
    filter.value = '';
    const items = filter.items.filter(it => {
      return it.selected;
    });
    if (items.length) {
      // filter.title = '';
      items.map((it, i) => {
        if (i !== items.length - 1) {
          filter.value += it.value + ',';
          // filter.title += it.name + ',';
        } else {
          filter.value += it.value;
          // filter.title += it.name;
        }
      });
    }
    this.getCount();
  }

  getCount() {
    this.countIsLoading = true;
    this.count_service.get(
      this.data.city_id,
      this.data.action_id,
      this.data.obj_id,
      this.mainFilters,
      this.filters
    );
  }


  constructor(private count_service: FiltersAdCountService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscriptions.add(this.count_service.value.subscribe((value: any) => {
      this.count = value;
      setTimeout(() => {
        this.countIsLoading = false;
      }, 150);
    }));
  }

}
