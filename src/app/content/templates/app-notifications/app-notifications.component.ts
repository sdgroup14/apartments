import {Component, NgZone, OnInit} from '@angular/core';
import {CommonAppService} from '../../../services/common-app.service';

@Component({
  selector: 'app-app-notifications',
  templateUrl: './app-notifications.component.html',
  styleUrls: ['./app-notifications.component.scss']
})
export class AppNotificationsComponent implements OnInit {
  isOpen = false;
  type = 'text';
  content = '';
  timeout: any;
  closeTimer = 4000;
  color = '#AEB6C9';
  progress = 100;

  constructor(
    public service: CommonAppService,
    private zone: NgZone
  ) {
  }

  close() {
    clearInterval(this.timeout);
    this.timeout = null;
    this.isOpen = false;
    setTimeout(() => {
      this.content = '';
      this.type = 'text';
      this.progress = 0;
    }, 300);
  }

  startTimer() {
    if (!this.timeout) {
      this.timeout = setTimeout(() => {
        this.close();
      }, this.closeTimer);
    }
  }

  ngOnInit() {
    this.service.notification.subscribe((data: any) => {
      if (data) {
        this.zone.run(() => {
          this.progress = 100;
          this.type = data.type;
          this.content = data.text;
          this.color = this.type === 'succsess' ? '#1DCC00' : this.type === 'warning' ? '#F20F0F' : '#AEB6C9';

          // '__warning': type === 'warning',
          //   '__succsess': type === 'succsess',
          //   '__text'
          this.isOpen = true;
          this.startTimer();
        });
      }
    });

  }

}
