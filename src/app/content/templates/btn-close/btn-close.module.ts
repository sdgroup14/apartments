import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BtnCloseComponent} from './btn-close.component';

@NgModule({
  declarations: [BtnCloseComponent],
  imports: [
    CommonModule
  ],
  exports: [BtnCloseComponent]
})
export class BtnCloseModule { }
