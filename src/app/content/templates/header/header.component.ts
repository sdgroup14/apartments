import {Component, NgZone, OnInit} from '@angular/core';
import {LangService} from '../../../services/lang.service';
import {CookieService} from '../../../services/cookies-service.service';
import {Location} from '@angular/common';
import {AppAuthService} from '../../../services/app-auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLangDropdown = false;
  isPlaceDropdown = false;
  place: any = {
    title: 'Бердянск'
  };
  currentLang: string;
  isLogin = true;

  user: any = {};
  langs: any = [
    {
      id: 'ru',
      title: 'рус',
      full_title: 'Русский',
      selected: true,
    },
    {
      id: 'uk',
      title: 'укр',
      full_title: 'Украинский',
      selected: false
    }
  ];

  closeLangDropdown() {
    if (this.isLangDropdown) {
      this.isLangDropdown = false;
    }
  }

  closePlaceDropdown() {
    if (this.isPlaceDropdown) {
      this.isPlaceDropdown = false;
    }
  }


  switchLang(lang) {
    // this.closeLangDropdown();
    // this.currentLang = lang.title;
    this.langService.switch(lang.id);
  }

  constructor(
    public langService: LangService,
    private cookie: CookieService,
    private location: Location,
    private zone: NgZone,
    public appAuth: AppAuthService
  ) {
  }

  ngOnInit() {
    // this.langService.get();

    this.appAuth.currentUserSubject.subscribe(data => {
      if (data) {
        this.zone.run(() => {
          this.isLogin = true;
          this.user = data.user;
          // console.log(this.user);
          // this.authCheck.emit(false);
          // this._renderer.removeClass(document.body, 'scrollOff');
        });
      } else {
        this.isLogin = false;
        this.user = {};
      }
    });

    if (this.appAuth.currentUserValue) {
      this.isLogin = true;
      this.user = this.appAuth.currentUserValue.user;
    }


    let current_lang = '';
    const _lang = this.langService.get();

    if (!_lang) {
      let count: any;
      const _d = new Date();
      const exp = _d.setFullYear(_d.getFullYear() + 1);
      const d_final = new Date(exp);
      if (!this.cookie.get('applangCounter')) {
        this.cookie.put('applangCounter', '0', {
          expires: d_final
        });
      } else {
        count = this.cookie.get('applangCounter');
        this.cookie.put('applangCounter', (+count + 1).toString(), {
          expires: d_final
        });
      }
      // this.router.events.subscribe((event: any) => {
      //   if (event instanceof NavigationEnd) {
      const routerLang: any = this.location.path().split('/')[1];
      // this.lang = routerLang === 'ru' ? false : true;
      current_lang = routerLang === 'ru' ? 'ru' : 'uk';
      // console.log(current_lang)
      if (count >= 4) {
        this.cookie.put('applang', current_lang, {
          expires: d_final
        });
        // this.checkLangCookie = true;
      }
    } else {
      current_lang = _lang;
    }
    this.langs.map(lang => {
      if (lang.id === current_lang) {
        this.currentLang = lang.title;
        lang.selected = true;
      } else {
        lang.selected = false;
      }
    });


    // }
    // });
  }

}
