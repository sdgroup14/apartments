import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-form-radio-multi-line',
  templateUrl: './form-radio-multi-line.component.html',
  styleUrls: ['./form-radio-multi-line.component.scss']
})
export class FormRadioMultiLineComponent implements OnInit, OnChanges {
  @Input() values: any = [];
  @Input() main: boolean;
  @Output() valueChange = new EventEmitter();
  // init = false;
  // btns: any = [
  //   {
  //     title: 'Купить',
  //     selected: false
  //   },
  //   {
  //     title: 'Арендовать',
  //     selected: true
  //   },
  //   {
  //     title: 'Посуточно',
  //     selected: false
  //   }
  // ];

  ngOnChanges(changes: SimpleChanges): void {
    if (this.main) {
      if (changes.values.currentValue) {
        const sel_btn = this.values.filter(b => {
          return b.is_def;
        })[0];
        if (sel_btn) {
          this.selectItem(sel_btn);
        }

        // this.count = this.maxLength - this.value.length;
      }
    }

  }

  selectItem(btn) {
    btn.selected = true;
    this.values.map((b, _i) => {
      if (btn.name !== b.name) {
        b.selected = false;
      }
    });
    this.valueChange.emit(btn);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
