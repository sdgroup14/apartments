import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormRadioMultiLineComponent} from './form-radio-multi-line.component';



@NgModule({
  declarations: [FormRadioMultiLineComponent],
  imports: [
    CommonModule
  ],
  exports: [FormRadioMultiLineComponent]
})
export class FormRadioMultiLineModule { }
