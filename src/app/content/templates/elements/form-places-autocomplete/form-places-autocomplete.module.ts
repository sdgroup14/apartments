import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormPlacesAutocompleteComponent} from './form-places-autocomplete.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormPlacesAutocompleteComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [FormPlacesAutocompleteComponent]
})
export class FormPlacesAutocompleteModule { }
