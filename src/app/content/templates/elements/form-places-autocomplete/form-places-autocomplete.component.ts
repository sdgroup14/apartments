import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnInit, Output, ViewChild} from '@angular/core';

import {} from 'googlemaps';
import {LangService} from '../../../../services/lang.service';
import {HttpClient} from '@angular/common/http';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-form-places-autocomplete',
  templateUrl: './form-places-autocomplete.component.html',
  styleUrls: ['./form-places-autocomplete.component.scss']
})
export class FormPlacesAutocompleteComponent implements OnInit, AfterViewInit {
  @Input() adressType: string;
  @Input() value: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext', {static: false}) addresstext: any;

  // autocompleteInput: string;
  queryWait: boolean;

  constructor(private zone: NgZone, private lang: LangService, private http: HttpClient) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.getPlaceAutocomplete();
  }

  // Initialize the map.
  // function initMap() {
  //   var map = new google.maps.Map(document.getElementById('map'), {
  //     zoom: 8,
  //     center: {lat: 40.72, lng: -73.96}
  //   });
  //   var geocoder = new google.maps.Geocoder;
  //   var infowindow = new google.maps.InfoWindow;
  //
  //   document.getElementById('submit').addEventListener('click', function() {
  //     geocodePlaceId(geocoder, map, infowindow);
  //   });
  // }
  //
  // // This function is called when the user clicks the UI button requesting
  // // a geocode of a place ID.
  // function geocodePlaceId(geocoder, map, infowindow) {
  //   var placeId = document.getElementById('place-id').value;
  //   geocoder.geocode({'placeId': placeId}, function(results, status) {
  //     if (status === 'OK') {
  //       if (results[0]) {
  //         map.setZoom(11);
  //         map.setCenter(results[0].geometry.location);
  //         var marker = new google.maps.Marker({
  //           map: map,
  //           position: results[0].geometry.location
  //         });
  //         infowindow.setContent(results[0].formatted_address);
  //         infowindow.open(map, marker);
  //       } else {
  //         window.alert('No results found');
  //       }
  //     } else {
  //       window.alert('Geocoder failed due to: ' + status);
  //     }
  //   });
  // }
  private getPlaceAutocomplete() {
    // const geocoder = new google.maps.Geocoder;
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
        componentRestrictions: {country: 'UA'},
        // types: ['geocode', 'establishment', 'address']
        types: ['address']
        // types: ['cities'], // 'establishment' / 'address' / 'geocode'
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place: any = autocomplete.getPlace();
      const town: any = {};
      // console.log(place)

      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + place.geometry.location.lat() + ',' + place.geometry.location.lng() + '&result_type=political&key=AIzaSyAg3VBgigtNX45T0brACRs6nhIxtNJFgx0').pipe(take(1)).subscribe((resp: any) => {
        console.log('resp', resp);
        // place
        resp.results.map(it => {
          const l = it.types.filter(type => {
            return type === 'locality';
          })[0];

          if (l) {
            town.place_id = it.place_id;
            town.lat = it.geometry.location.lat;
            town.lng = it.geometry.location.lng;
            // console.log(it.place_id)
            it.address_components.map(_it => {
              // console.log(it);
              const _l = _it.types.filter(type => {
                return type === 'locality';
              })[0];

              if (_l) {
                // console.log(it.short_name);
                town.long_name = _it.long_name;
                // console.log(place)
              }
            });
            // console.log(it.short_name);
            // this.service.getCity(it.short_name).pipe(take(1)).subscribe((resp: any) => {
            //   console.log(resp);
            //   this.districts = resp.result.districts;
            //   this.metro = resp.result.metro;
            // });
            // // console.log(place)
          }
        });

        // console.log(town);

        this.zone.run(() => {
          this.invokeEvent({place, town});
        });
      });
      // geocoder.geocode({placeId: place.place_id}, (results, status: any) => {
      //   if (status === 'OK') {
      //     console.log(results);
      //   }
      // });
    });
  }

  invokeEvent(data) {
    // console.log(data);
    this.setAddress.emit(data);
  }

}
