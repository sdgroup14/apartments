import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-radio',
  templateUrl: './form-radio.component.html',
  styleUrls: ['./form-radio.component.scss']
})
export class FormRadioComponent implements OnInit {
  values: any = [];

  @Input() set setValues(data) {
    // console.log(data);
    this.values = data;
    const def_item = this.values.filter(it => {
      return it.is_def;
    })[0];
    if (def_item) {
      setTimeout(() => {
        this.selectItem(def_item);
      }, 1);
    }
  }

  @Output() valueChange = new EventEmitter();
  // btns: any = [
  //   {
  //     title: 'Купить',
  //     selected: false
  //   },
  //   {
  //     title: 'Арендовать',
  //     selected: true
  //   },
  //   {
  //     title: 'Посуточно',
  //     selected: false
  //   }
  // ];

  selectItem(btn) {
    btn.selected = true;
    this.values.map((b, _i) => {
      if (btn.value !== b.value) {
        b.selected = false;
      }
    });
    this.valueChange.emit(btn);
  }

  constructor() {
  }


  ngOnInit() {
  }

}
