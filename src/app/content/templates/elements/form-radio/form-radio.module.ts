import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormRadioComponent} from './form-radio.component';



@NgModule({
  declarations: [FormRadioComponent],
  imports: [
    CommonModule
  ],
  exports: [FormRadioComponent]
})
export class FormRadioModule { }
