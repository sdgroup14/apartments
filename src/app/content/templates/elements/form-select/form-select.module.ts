import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormSelectComponent} from './form-select.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ClickOutsideModule} from '../../../../directives/click-outside/click-outside.module';



@NgModule({
  declarations: [FormSelectComponent],
  imports: [
    CommonModule,
    ClickOutsideModule,
    PerfectScrollbarModule
  ],
  exports: [FormSelectComponent]
})
export class FormSelectModule { }
