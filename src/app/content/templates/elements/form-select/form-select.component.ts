import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss']
})
export class FormSelectComponent implements OnInit {

  @Input() list: any = [];
  @Input() width: string;
  @Input() key: string;
  @Output() changeValue = new EventEmitter();
  isOpen = false;
  value: string;

  @Input() set setValue(arr) {
    if (arr && arr.length) {
      const selected = arr.filter(it => {
        return it.selected;
      })[0];
      if (selected) {
        this.change(selected);
      }
    }
  };

  close() {
    this.isOpen = false;
  }

  change(item) {
    this.isOpen = false;
    this.value = item[this.key];
    this.changeValue.emit(item);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
