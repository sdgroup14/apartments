import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormPhotoComponent} from './form-photo.component';
import {SafeUrlPipeModule} from '../../../../pipes/safe-url/safe-url-pipe.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {OrderByModule} from '../../../../pipes/order-by/order-by.module';


@NgModule({
  declarations: [FormPhotoComponent],
  imports: [
    CommonModule,
    SafeUrlPipeModule,
    DragDropModule,
    PerfectScrollbarModule,
    OrderByModule
  ],
  exports: [FormPhotoComponent]
})
export class FormPhotoModule { }
