import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AddApartmentPageService} from '../../../pages/add-apartment-page/services/add-apartment-page.service';

@Component({
  selector: 'app-form-photo',
  templateUrl: './form-photo.component.html',
  styleUrls: ['./form-photo.component.scss']
})
export class FormPhotoComponent implements OnInit {
  @Input() imgs: any = [];
  @Input() formats: any;
  @Input() maxSize: number;
  @Input() minWidth: number;
  @Output() newImgs = new EventEmitter();

  // imgs: any = [];

  error = '';

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.imgs, event.previousIndex, event.currentIndex);
    // this.activeSlide = this.slides[this.i];
    this.imgs.map((slide, i) => {
      slide.order = i;
    });
    this.newImgs.emit(this.imgs);
  }

  deleteSlide(slide) {
    if (slide.hasOwnProperty('id')) {
      this.service.deleteSlide(slide.id).subscribe(resp => {
        this.error = '';
        const index = this.imgs.indexOf(slide);
        this.imgs.splice(index, 1);
        this.newImgs.emit(this.imgs);
        // if (index === this.i) {
        //   this.swiper.directiveRef.swiper().slideTo(0);
        //
        // }
      });
    } else {
      this.error = '';
      const index = this.imgs.indexOf(slide);
      this.imgs.splice(index, 1);
      this.newImgs.emit(this.imgs);
    }
  }

  getPhoto(e) {
    for (const file of e.target.files) {

      // const file = e.target.files[0];
      if (file) {
        const fileExtension = file.name.replace(/^.*\./, '');
        const checkFormat = this.formats.filter(format => {
          return format === fileExtension.toLowerCase();
        });
        if (checkFormat.length) {
          if (file.size > this.maxSize) {
            this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
              this.error = data + ' (' + this.maxSize / 1000000 + 'мб)';
            });
          } else {
            const reader = new FileReader(); // CREATE AN NEW INSTANCE.
            const that = this;
            reader.onload = (_e: any) => {
              const img = new Image();
              img.src = _e.target.result;

              img.onload = () => {
                const w = img.width;
                if (w < this.minWidth) {
                  this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
                    that.error = data + ' ' + this.minWidth + 'px';
                  });
                } else {
                  // console.log(file);
                  // console.log(this.imgs);
                  if (!this.imgs) {
                    this.imgs = [];
                  }
                  this.imgs.push({
                    img: file,
                    order: this.imgs.length
                  });
                  // this.img = file;
                  that.newImgs.emit(this.imgs);
                  e.target.value = '';
                }
              };
            };
            reader.readAsDataURL(file);
          }
        } else {
          this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
            this.error = data + ' (' + this.formats.join(', ') + ')';
          });
        }
      }
    }
    // e.target.files.map(file => {
    //
    // });
    this.error = null;

  }

  constructor(private translate: TranslateService, private service: AddApartmentPageService) {
  }

  ngOnInit() {
  }

}
