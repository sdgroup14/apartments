import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';

import {} from 'googlemaps';

@Component({
  selector: 'app-form-map',
  templateUrl: './form-map.component.html',
  styleUrls: ['./form-map.component.scss']
})
export class FormMapComponent implements OnInit, AfterViewInit {
  @ViewChild('map', {static: false}) mapElement: any;
  map: google.maps.Map;
  marker: any;

  @Input() set setView(place) {
    if (this.marker) {
      this.marker.setVisible(false);
    }
    if (!place.hasOwnProperty('lng')) {
      return;

    }
    // if (!place.geometry) {
    //   // User entered the name of a Place that was not suggested and
    //   // pressed the Enter key, or the Place Details request failed.
    //   // window.alert("No details available for input: '" + place.name + "'");
    //   return;
    // }
    // console.log('123', place.geometry.location.lat());
// console.log(place)
    const latlng = new google.maps.LatLng(place.lat, place.lng);
    // console.log(latlng)
    this.marker.setPosition(latlng);
    // this.map.setCenter(latlng)

    this.map.setCenter(latlng);
    this.map.setZoom(10);  // Why 17? Because it looks good.
    // this.marker.setPosition({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});
    this.marker.setVisible(true);


    // if (place.geometry.viewport) {
    //   // console.log('latlng')
    //   this.map.fitBounds(place.geometry.viewport);
    // } else {
    //   const latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
    //   // console.log(latlng)
    //   this.marker.setPosition(latlng);
    //   // this.map.setCenter(latlng)
    //
    //   this.map.setCenter(place.geometry.location);
    //   this.map.setZoom(17);  // Why 17? Because it looks good.
    //   // this.marker.setPosition({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});
    //   this.marker.setVisible(true);
    // }
  }

  constructor() {
  }

  ngAfterViewInit(): void {
    const mapProperties = {
      center: new google.maps.LatLng(48.3140961, 28.9368704),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
    this.map.addListener('click', (e) => {
      console.log(e);
    });
    // setTimeout(() => {
    //   console.log('set marker');
    this.marker = new google.maps.Marker({
      position: {lat: 35.2271, lng: -80.8431},
      map: this.map
    });
    this.marker.setVisible(false);
    // const latlng = new google.maps.LatLng(-24.397, 140.644);
    // this.marker.setPosition(latlng);
    // this.map.setCenter(latlng)
    // this.marker = new google.maps.Marker({
    //   map: this.map,
    //   anchorPoint: new google.maps.Point(35.2271, -80.8431)
    // });
    // }, 3000);

  }

  ngOnInit() {

  }

}
