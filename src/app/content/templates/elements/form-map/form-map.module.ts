import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormMapComponent} from './form-map.component';



@NgModule({
  declarations: [FormMapComponent],
  imports: [
    CommonModule
  ],
  exports: [FormMapComponent]
})
export class FormMapModule { }
