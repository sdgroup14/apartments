import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormCheckboxComponent} from './form-checkbox.component';



@NgModule({
  declarations: [FormCheckboxComponent],
  imports: [
    CommonModule
  ],
  exports: [FormCheckboxComponent]
})
export class FormCheckboxModule { }
