import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-checkbox',
  templateUrl: './form-checkbox.component.html',
  styleUrls: ['./form-checkbox.component.scss']
})
export class FormCheckboxComponent implements OnInit {
  @Input() values: any = [];
  @Output() valueChange = new EventEmitter();
  // btns: any = [
  //   {
  //     title: 'Купить',
  //     selected: false
  //   },
  //   {
  //     title: 'Арендовать',
  //     selected: true
  //   },
  //   {
  //     title: 'Посуточно',
  //     selected: false
  //   }
  // ];

  selectItem(btn) {
    const selected_items = this.values.filter(b => {
      return b.selected;
    });

    // if (this.single && selected_items.length && !btn.selected) {
    //   return;
    // }
    btn.selected = !btn.selected;
    this.valueChange.emit(true);
    // this.values.map((b, _i) => {
    //   if (btn.value !== b.value) {
    //     b.selected = false;
    //   }
    // });
  }

  constructor() {
  }

  ngOnInit() {
  }

}
