import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-input-string',
  templateUrl: './form-input-string.component.html',
  styleUrls: ['./form-input-string.component.scss']
})
export class FormInputStringComponent implements OnInit  {
  @Input() placeholder: string;
  @Input() width: string;
  @Output() valueChange = new EventEmitter();
  @Input() value: string;

  constructor() {
  }

  ngOnInit() {
  }

}
