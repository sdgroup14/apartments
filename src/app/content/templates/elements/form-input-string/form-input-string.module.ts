import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormInputStringComponent} from './form-input-string.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormInputStringComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [FormInputStringComponent]
})
export class FormInputStringModule { }
