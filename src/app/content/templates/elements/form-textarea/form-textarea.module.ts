import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormTextareaComponent} from './form-textarea.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormTextareaComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [FormTextareaComponent]
})
export class FormTextareaModule { }
