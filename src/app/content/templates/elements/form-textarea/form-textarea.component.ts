import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-form-textarea',
  templateUrl: './form-textarea.component.html',
  styleUrls: ['./form-textarea.component.scss']
})
export class FormTextareaComponent implements OnInit, OnChanges {
  maxLength = 1000;
  count = this.maxLength;
  @Input() placeholder: string;
  @Output() valueChange = new EventEmitter();
  @Input() value: string;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.value.currentValue) {
      this.count = this.maxLength - this.value.length;
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
