import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-input-number',
  templateUrl: './form-input-number.component.html',
  styleUrls: ['./form-input-number.component.scss']
})
export class FormInputNumberComponent implements OnInit {
  @Input() label: string;
  @Input() placeholder: string;
  @Input() width: string;
  @Input() align: string;
  @Output() valueChange = new EventEmitter();
  @Input() value: string;

  constructor() {
  }

  ngOnInit() {
  }

}
