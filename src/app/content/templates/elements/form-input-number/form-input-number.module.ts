import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormInputNumberComponent} from './form-input-number.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [FormInputNumberComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [FormInputNumberComponent]
})
export class FormInputNumberModule {
}
