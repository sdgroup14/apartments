import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormInputPhoneComponent} from './form-input-phone.component';
import {NgxMaskModule} from 'ngx-mask';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormInputPhoneComponent],
  imports: [
    CommonModule,
    NgxMaskModule,
    FormsModule
  ],
  exports: [FormInputPhoneComponent]
})
export class FormInputPhoneModule { }
