import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-input-phone',
  templateUrl: './form-input-phone.component.html',
  styleUrls: ['./form-input-phone.component.scss']
})
export class FormInputPhoneComponent implements OnInit {
  @Input() placeholder: string;
  @Input() width: string;
  @Output() valueChange = new EventEmitter();
  @Input() value: string;


  constructor() { }

  ngOnInit() {
  }

}
