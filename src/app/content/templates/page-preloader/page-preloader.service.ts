import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PagePreloaderService {
  visibility = new Subject();
  interval: any;

  show() {
    clearInterval(this.interval);
    this.visibility.next(true);
  }

  hide() {
    this.interval = setTimeout(() => {
      this.visibility.next(false);
    }, 300);
  }


  constructor() {
  }
}
