import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonAppService} from '../../../services/common-app.service';
import {Subscription} from 'rxjs';
import {NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.scss']
})
export class TopNavigationComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  list: any = [];

  constructor(private common: CommonAppService, private router: Router) {
    // router.events.subscribe(e => {
    //   if (e instanceof NavigationStart) {
    //     // this.list = [];
    //     // console.log(e);
    //   }
    // });
  }

  ngOnDestroy() {
    // this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }


  ngOnInit() {
    this.subscriptions.add(this.common.getTopNav().subscribe((resp: any) => {
      // console.log(resp.result.top)
      // if (nav.length) {
      //   // console.log('nav ', nav);
      this.list = resp.result.top;
      // }
    }));
  }

}
