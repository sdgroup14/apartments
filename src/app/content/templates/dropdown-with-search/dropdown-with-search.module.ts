import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DropdownWithSearchComponent} from './dropdown-with-search.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [DropdownWithSearchComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule
  ],
  exports: [DropdownWithSearchComponent]
})
export class DropdownWithSearchModule {
}
