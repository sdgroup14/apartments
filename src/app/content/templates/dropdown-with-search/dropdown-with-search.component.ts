import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DropdownWithSearchService} from './dropdown-with-search.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-dropdown-with-search',
  templateUrl: './dropdown-with-search.component.html',
  styleUrls: ['./dropdown-with-search.component.scss']
})
export class DropdownWithSearchComponent implements OnInit {
  @Output() closeDropdown = new EventEmitter();
  @Output() changeValue = new EventEmitter();
  @Input() url: string;
  @Input() key: string;
  @Input() key_dropdown: string;
  @ViewChild('searchEl', {static: false}) searchEl: ElementRef;

  @Input() set open(boolean) {
    if (boolean) {
      this.search();
      setTimeout(() => {
        this.searchEl.nativeElement.focus();
      });

    }
  }

  value = '';
  list: any = [];

  search() {
    this.service.get(this.url, this.value).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      this.list = resp.result;
    });
  }

  change(item) {
    this.changeValue.emit(item);
  }


  constructor(private service: DropdownWithSearchService) {
  }

  ngOnInit() {
  }

}
