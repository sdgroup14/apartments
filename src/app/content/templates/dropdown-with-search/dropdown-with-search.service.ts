import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DropdownWithSearchService {
  constructor(private  http: HttpClient) {
  }

  get(url, str) {
    // const query = '';
    const query = str ? '?search=' + str : '';
    return this.http.get(environment.res_api_url + url + query);
  }
}
