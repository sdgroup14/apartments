import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-save-preloader',
  templateUrl: './save-preloader.component.html',
  styleUrls: ['./save-preloader.component.scss']
})
export class SavePreloaderComponent implements OnInit {
  isVisible: any = false;

  constructor() {
  }

  ngOnInit() {
    setTimeout(() => {
      this.isVisible = true;
    });
    // this.service.visibility.subscribe(bool => {
    //   this.isVisible = bool;
    // });
    // this.__common.preloader.subscribe((bool: any) => {
    //   this.isPreloader = bool;
    // });
  }

}
