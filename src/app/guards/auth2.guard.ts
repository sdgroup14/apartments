import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AppAuthService} from '../services/app-auth.service';
import {CommonAppService} from '../services/common-app.service';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardLogOut implements CanActivate {

  constructor(
    private auth: AppAuthService,
    private translate: TranslateService,
    private router: Router,
    private _common: CommonAppService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.auth.currentUserValue;
    if (currentUser) {
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
