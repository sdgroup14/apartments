import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'phone'
})
export class PhonePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {

  }

  transform(value: any, ...args: any[]): any {
    // this.trustedUrl =
    return this.sanitizer.bypassSecurityTrustUrl(value);
  }

}
