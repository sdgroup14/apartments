import {NgModule} from '@angular/core';
import {BrowserModule, TransferState} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {TransferHttpCacheModule} from '@nguniversal/common';
import {HeaderComponent} from './content/templates/header/header.component';
import {FooterComponent} from './content/templates/footer/footer.component';
import {ClickOutsideModule} from './directives/click-outside/click-outside.module';
import {DropdownWithSearchModule} from './content/templates/dropdown-with-search/dropdown-with-search.module';
import {TopNavigationComponent} from './content/templates/top-navigation/top-navigation.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PagePreloaderModule} from './content/templates/page-preloader/page-preloader.module';
import {AuthModalComponent} from './content/templates/auth-modal/auth-modal.component';
import {BtnCloseModule} from './content/templates/btn-close/btn-close.module';
import {TranslateBrowserLoader} from './services/translate-browser-loader.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {CheckboxModule} from './content/templates/checkbox/checkbox.module';
import {BrowserCookiesModule} from '@ngx-utils/cookies/browser';
import {AuthPageComponent} from './content/pages/auth-page/auth-page.component';
import {JwtInterceptor} from './interceptors/http.interceptor';
import {NgxMaskModule} from 'ngx-mask';
import {SavePreloaderComponent} from './content/templates/save-preloader/save-preloader.component';
import {AppNotificationsComponent} from './content/templates/app-notifications/app-notifications.component';
import {NgCircleProgressModule} from 'ng-circle-progress';

export function exportTranslateStaticLoader(http: HttpClient, transferState: TransferState) {
  return new TranslateBrowserLoader('/assets/i18n/', '.json', transferState, http);
}

@NgModule({
  imports: [
    BrowserModule.withServerTransition({appId: 'app'}),
    TransferHttpCacheModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ClickOutsideModule,
    DropdownWithSearchModule,
    BrowserAnimationsModule,
    PagePreloaderModule,
    BtnCloseModule,
    CheckboxModule,
    NgxMaskModule.forRoot(),
    // ShearedModule,
    // FeedbackModalModule,
    // ContradictionModalModule,
    // BtnCloseModule,
    // ClickOutsideModule,
    // SafeHtmlPipeModule,
    // BrowserAnimationsModule,
    // CheckboxModule,
    // PagePreloaderModule,
    // AddPersonModalModule,
    // SheredTooltipModule,
    BrowserCookiesModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: exportTranslateStaticLoader,
        deps: [HttpClient, TransferState]
      }
    }),
    NgCircleProgressModule.forRoot()
    // AngularFireModule.initializeApp(_config),
    // AngularFireAuthModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TopNavigationComponent,
    AuthModalComponent,
    AuthPageComponent,
    SavePreloaderComponent,
    AppNotificationsComponent
  ],
  providers: [
    // CommonAppService,
    // PlatformService,
    // LangService,
    // CookieService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
