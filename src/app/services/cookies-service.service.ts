import {Inject, Injectable, Optional} from '@angular/core';
import {REQUEST} from '@nguniversal/express-engine/tokens';
import {CookiesService} from '@ngx-utils/cookies';
import {PlatformService} from './platform.service';

@Injectable({
  providedIn: 'root'
})
export class CookieService {

  constructor(
    @Optional() @Inject(REQUEST) protected request: any,
    private cookie: CookiesService,
    private platform: PlatformService
  ) {
  }

  get(key) {
    let value: any;
    if (this.platform.check()) {
      value = this.cookie.get(key);
    } else {
      if (Object.keys(this.request.cookies).length && this.request.cookies.hasOwnProperty(key)) {
        // console.log(this.request.cookies[key]);
        value = this.request.cookies[key];
      }
      // this.request.cookies
    }
    return value;
  }


  put(key, value, opt: any) {
    return this.cookie.put(key, value, {path: '/', expires: opt.expires});
  }

  remove(key) {
    return this.cookie.remove(key);
  }


}
