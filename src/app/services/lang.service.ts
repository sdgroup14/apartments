import {Injectable} from '@angular/core';
import {CookieService} from './cookies-service.service';
// import {Location} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class LangService {

  switch(lang) {
    if (this.cookie.get('applang') === lang) {
      return;
    }
    this.cookie.remove('applang');
    const _d = new Date();
    const exp = _d.setFullYear(_d.getFullYear() + 1);
    const d_final = new Date(exp);
    this.cookie.put('applang', lang, {
      expires: d_final
    });
    window.location.reload();
  }

  get() {
    // if (this.cookie.get('applang')) {
    return this.cookie.get('applang');
    // }

    // const routerLang: any = this.location.path().split('/')[1];
    // const _lang = routerLang === 'ru' ? 'ru' : 'uk';
    // return _lang;
  }

  check() {
  }

  constructor(private cookie: CookieService) {
  }
}
