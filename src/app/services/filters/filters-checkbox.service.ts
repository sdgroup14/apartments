import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FiltersCheckboxService {

  change(data, filter) {
    filter.value = '';
    const items = filter.items.filter(it => {
      return it.selected;
    });
    if (items.length) {
      filter.title = '';
      items.map((it, i) => {
        if (i !== items.length - 1) {
          filter.value += it.value + ',';
          filter.title += it.name + ',';
        } else {
          filter.value += it.value;
          filter.title += it.name;
        }
      });
    } else {
      filter.title = filter.placeholder;
    }
    // filter.value = data.id;
    // console.log(filter);
    // this.selectObject.emit(data);
  }

  constructor() { }
}
