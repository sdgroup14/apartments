import {Injectable} from '@angular/core';
import {take} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FiltersObjectService {

  changeObject(data, filters, result_data, props) {
    const obj = data.obj;
    const filter = data.filter;
    result_data.obj_slug = obj.slug;
    result_data.obj_id = obj.id;
    const ind = filters.indexOf(filter);
    filters.length = ind + 1;
    if (obj.is_final) {
      setTimeout(() => {
        obj.dep_slug.map(key => {
          // console.log(key);
          const new_item = props[key];
          new_item.isProp = true;
          if (new_item) {
            filters.push(props[key]);
          }
        });
      }, 1);
    } else {
      console.log('here');
      this.getGroups(result_data.action_slug, result_data.obj_slug).pipe(take(1)).subscribe((resp: any) => {
        setTimeout(() => {
          resp.result.items = [resp.result.items];
          filters.push(resp.result);
        }, 1);
      });
    }
  }

  selectObjectItem(obj, row, filter) {
    filter.items.map(r => {
      if (r.slug !== row.slug) {
        // console.log(r);
        r.items.map(it => {
          it.selected = false;
        });
      }

    });
    filter.value = obj.id;
    filter.title = obj.name;
    filter.d_isOpen = false;
  }

  // getGroups() {

  // }

  getGroups(action_slug, group_slug) {
    const urlParams = '?action_slug=' + action_slug + '&group_slug=' + group_slug;
    return this.http.get(environment.res_api_url + '/search-widget/group-objects-list' + urlParams);
  }

  constructor(private http: HttpClient) {
  }
}
