import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {take} from 'rxjs/operators';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FiltersAdCountService {
  value = new BehaviorSubject(0);

  reqHandler(city_id, action_id, obj_id, filters, ext_filters) {
    // const urlParams = '';
    let urlParams = city_id ? '?city_id=' + city_id + '&' : '?';
    urlParams += action_id ? 'action_id=' + action_id : '';
    urlParams += obj_id ? '&obj_id=' + obj_id : '';
    urlParams += '&filters=' + JSON.stringify(filters);
    urlParams += '&ext_filters=' + JSON.stringify(ext_filters);
    // const urlParams = '?action_slug=' + action_slug + '&group_slug=' + group_slug;
    return this.http.get(environment.res_api_url + '/search-widget/ads-count' + urlParams);
  }

  get(city_id, action_id, obj_id, mainFilters, moreFilters) {
    // console.log(mainFilters);
    const ext = {};
    moreFilters.filter(filter => {
      return filter.value;
    }).map(f => {
      ext[f.alias] = f.value;
    });
    const main = {};
    mainFilters.filter(filter => {
      return filter.hasOwnProperty('alias');
    }).map(f => {
      console.log('123', f.value)
      main[f.slug] = f.value;
    });

    this.reqHandler(
      city_id,
      action_id,
      obj_id,
      main,
      ext).pipe(take(1)).subscribe((resp: any) => {
      this.value.next(resp.result.count);
    });
  }

  constructor(private http: HttpClient) {
  }
}
