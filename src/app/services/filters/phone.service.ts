import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhoneService {

  getPhone(ad, action_id) {
    // console.log(ad);
    if (!ad.check_phone) {
      this.get(action_id, ad.id).subscribe((resp: any) => {
        // console.log(resp);
        ad.phone = resp.result;
        ad.check_phone = true;
      });
    }
  }

  get(action_id, ad_id) {
    let urlParams = action_id ? '?action_id=' + action_id + '&' : '?';
    urlParams += ad_id ? 'ad_id=' + ad_id : '';
    return this.http.get(environment.api + '/show-phone' + urlParams);
  }


  // https://nm-api.wander.black/v1/filters
  constructor(private http: HttpClient) {
  }
}
