import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor, HttpParams,
  HttpRequest
} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
import {Location} from '@angular/common';
import {LangService} from '../services/lang.service';
import {environment} from '../../environments/environment';
import {AppAuthService} from '../services/app-auth.service';
import {CookieService} from '../services/cookies-service.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  newUser: any = {};

  constructor(
    private langService: LangService,
    private router: Router,
    private appAuth: AppAuthService,
    private cookie: CookieService,
    private location: Location) {
  }

  isRefreshingToken = false;

  intercept(request: HttpRequest<any>, next: HttpHandler): any {
    let newParams = new HttpParams({fromString: request.params.toString()});
    let lang = this.langService.get();
    this.appAuth.checkExpares();
    const currentUser: any = this.appAuth.currentUserValue;
    // this.newUser = currentUser.user;

    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          Authorization: currentUser.token
        }
      });
    }

    if (lang) {
      lang = lang === 'uk' ? 'ua' : 'ru';
    } else {
      const checkRU = this.location.path().split('/')[1];
      lang = checkRU === 'ru' ? 'ru' : 'ua';
    }

    newParams = newParams.append('lang', lang);
    const requestClone = request.clone({
      params: newParams
    });

    return next.handle(requestClone).pipe(catchError(err => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          if (request.url === `${environment.res_api_url}/auth/refresh`) {
            this.appAuth.clear();
          }
          // return this.handleUnauthorized(request, next);
          return throwError(err);
        } else if (err.status === 400) {
          // console.log('here', request.url);
          // console.log('here1', `${environment.res_api_url}/auth/refresh`);
          if (request.url === `${environment.res_api_url}/auth/refresh`) {
            this.appAuth.clear();
          } else {
            return this.handleUnauthorized(request, next);
          }
        }
        return throwError(err);
      } else {

        return throwError(err);
      }
    }));
  }

  handleUnauthorized(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      return this.appAuth.refreshToken()
        .pipe(switchMap((resp: any) => {
            const result: any = resp.result;
            const d_final = new Date(result.refresh_exp * 1000);
            this.cookie.put('appToken', JSON.stringify(result), {
              expires: d_final
            });
            this.appAuth.currentUserSubject.next(result);

            req = req.clone({
              setHeaders: {
                Authorization: result.token
              }
            });
            return next.handle(req);
          }), catchError(error => {
            return throwError(error);
          }), finalize(() => {
            this.isRefreshingToken = false;
          })
        );
    } else {
      return this.appAuth.currentUserSubject
        .pipe(
          filter(token => token != null),
          take(1),
          switchMap(() => {
            req = req.clone({
              setHeaders: {
                Authorization: this.appAuth.currentUserValue.token
              }
            });
            return next.handle(req);
          })
        );
    }
  }
}




