export const environment = {
  production: true,
  api: 'https://nm-api.wander.black/api/v1',
  res_api_url: 'https://nm-res.wander.black/api/v1',
  domain: 'https://democratium.net'
};
