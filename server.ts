// These are important and needed before anything else
import 'zone.js/dist/zone-node';
import 'reflect-metadata';

import {enableProdMode} from '@angular/core';

import * as express from 'express';
import {join} from 'path';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import {REQUEST, RESPONSE} from '@nguniversal/express-engine/tokens';
// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

const domino = require('domino');
const fs = require('fs');
const template$ = fs.readFileSync('dist/browser/index.html').toString();
const win = domino.createWindow(template$);

global['window'] = win;
global['document'] = win.document;
global['DOMTokenList'] = win.DOMTokenList;
global['Node'] = win.Node;
global['Text'] = win.Text;
global['HTMLElement'] = win.HTMLElement;
global['navigator'] = win.navigator;

// Express server
const app = express();
app.use(compression());
app.use(cookieParser());

const PORT = process.env.PORT || 5000;
const DIST_FOLDER = join(process.cwd(), 'dist');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./dist/server/main');

// Express Engine
// import { ngExpressEngine } from '@nguniversal/express-engine';
const template = readFileSync(join(DIST_FOLDER, 'browser', 'index.html')).toString();
// Import module map for lazy loading
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';
// import {renderModuleFactory} from '@angular/platform-server';
import {readFileSync} from 'fs';
import {ngExpressEngine} from '@nguniversal/express-engine';

// app.engine('html', (_, options, callback) => {
//   renderModuleFactory(AppServerModuleNgFactory, {
//     // Our index.html
//     document: template,
//     url: options.req.url,
//     // DI so that we can get lazy-loading to work differently (since we need it to just instantly render it)
//     extraProviders: [
//       provideModuleMap(LAZY_MODULE_MAP),
//       {
//         provide: 'REQUEST',
//         useValue: options.req
//       },
//       {
//         provide: 'RESPONSE',
//         useValue: options.req.res
//       }
//     ]
//   }).then(html => {
//     callback(null, html);
//   });
// });

app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));

// TODO: implement data requests securely
app.get('/api/*', (req, res) => {
  res.status(404).send('data requests are not supported');
});

// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));

// All regular routes use the Universal engine
app.get('*', (_req, _res) => {
  _res.render('index', {
    req: _req,
    res: _res,
    // provers from server
    providers: [
      // for http and cookies
      {
        provide: 'REQUEST',
        useValue: _req,
      },
      {
        provide: 'RESPONSE',
        useValue: _res,
      }
    ]
  })
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});
